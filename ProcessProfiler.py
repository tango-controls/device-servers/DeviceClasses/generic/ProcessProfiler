#!/usr/bin/python
#    "$Name:  $";
#    "$Header:  $";
# =============================================================================
#
# file :        ProcessProfiler.py
#
# description : Python source for the ProcessProfiler and its commands. 
#                The class is derived from Device. It represents the
#                CORBA servant object which will be accessed from the
#                network. All commands which can be executed on the
#                ProcessProfiler are implemented in this file.
#
# project :     TANGO Device Server
#
# $Author:  $
#
# $Revision:  $
#
# $Log:  $
#
# copyleft :    European Synchrotron Radiation Facility
#               BP 220, Grenoble 38043
#               FRANCE
#
# =============================================================================
#          This file is generated by POGO
#    (Program Obviously used to Generate tango Object)
#
#         (c) - Software Engineering Group - ESRF
# =============================================================================
#
import os
import re
import sys
import threading
import time
import traceback
import psutil

import tango

import fandango as fn
from fandango.functional import *
from fandango.linos import shell_command, get_memory

if 'PyDeviceClass' not in dir(tango):
    tango.PyDeviceClass = tango.DeviceClass

__doc__ = """
    ProcessProfiler Usage
    ---------------------
    
    # Create a new device ; preferably like sys/profile/${HOSTNAME}
    # Configure the properties with the list of processes to control:
    
    .. note: ProcessList:
                Starter
                notifd
                ProcessProfiler
      
    A sample code to do so would be::

        import fandango
        host = fn.linos.MyMachine().host
        #Create the device
        fn.tango.add_new_device('ProcessProfiler/%s'%host,'ProcessProfiler','sys/profile/%s'%host)
        #Setup properties
        fn.get_database().put_device_property('sys/profile/%s'%host,{'ProcessList':['Starter','notifd','ProcessProfiler']})
        #Start the device
        fn.Astor('sys/profile/%s'%host).start_servers(host=host)
        #Start the update() command polling
        time.sleep(15.)
        fn.get_device(sys/profile/%s'%host).poll_command('update',10000)
    """


def tracer(msg):
    print('%s: %s' % (time2str(), msg))


def memory_checker(f):
    def wrapper(*args, **kwargs):
        m0 = get_memory()
        v = f(*args, **kwargs)
        m = get_memory()
        diff = m - m0
        if diff > 0:
            print('In %s: ' % f.__name__ + '%s: + %s' % (m * 1e-6, (m - m0) * 1e-6))
        return v

    return wrapper


#@memory_checker
def get_all_process(regexp=''):
    """
    Returns a {int(PID):stats} dictionary
    """
    import re
    # tracer('get_all_process(%s)'%regexp)
    pss = {}
    try:
        comm = "ps hax -o pid,nlwp,%cpu,%mem,rss,vsize,cmd"  # hax is needed to show Starter process
        lines = shell_command(comm).split('\n')
        for r in map(str.split, lines):
            if not r: continue
            r = dict((k, v if k == 'cmd' else (float(v) if '.' in v else int(v))) for k, v in
                     zip(('pid', 'threads', 'cpu', 'mem', 'rss', 'vsize', 'cmd'), (r[0:6]) + [' '.join(r[6:])]))
            # r['rss'],r['vsize'] = 1e-3*r['rss'],1e-3*r['vsize'] #Memory read in MB
            r['rss'], r['vsize'] = r['rss'], r['vsize']  # Memory read in KBytes
            if r['pid'] and not regexp or re.search(regexp, r.get('cmd', '')):
                pss[int(r['pid'])] = r
    except:
        print(traceback.format_exc())
    return pss


def get_worse_process(processes=None, key='rss'):
    # processes must be {PID:dict} 
    all_proc = processes or get_all_process()
    return sorted((v[key], v['pid']) for v in all_proc.values())[-1][-1]


def getMemUsage(self, pid=None, virtual=False):
    """
    DEPRECATED, GET IT FROM self.all_proc
    """
    #tracer('getMemUsage(%s)' % pid)
    if pid is None:
        pid = self._PID
    tag = 'Vm%s' % ('Size' if virtual else 'RSS')
    # mem,units = shell_command('cat /proc/%s/status | grep Vm%s'%(pid,'Size' if virtual else 'RSS')).lower().strip().split()[1:3]
    f = open('/proc/%s/status' % pid, 'r')
    lines = f.readlines()
    f.close()
    l = (a for a in lines if tag in a).next()
    mem, units = l.lower().strip().split()[1:3]
    mem = int(mem) * (1e3 if 'k' in units else (1e6 if 'm' in units else 1))
    del lines
    return mem


class ProcessProfiler(fn.device.Dev4Tango):
    #tango.LatestDeviceImpl,fn.Logger):

    # --------- Add you global variables here --------------------------

    def getMemRate(self):
        try:
            # print('getMemRate()')
            meminfo = shell_command('cat /proc/meminfo').split('\n')
            meminfo = dict(map(str.strip, a.split(':', 1)) for a in meminfo if a.strip())
            getter = lambda k: float(
                eval(meminfo[k].lower().replace('kb', '*1').replace('mb', '*1e3').replace('b', '*1e-3')))
            self._total, self._free, self._cached = getter('MemTotal'), getter('MemFree'), getter('Cached')
            self._cached += getter('Slab') + getter('Buffers')
            ## Shared Memory Reclaimable could be considered also as Cached, but it is not 100% sure if it will be always reusable or not
            # Therefore, I prefered to ignore it. This is why this method may return less free memory than top.
            self._used = (self._total - (self._free + self._cached))
            self._memrate = (self._used / self._total)
        except:
            traceback.print_exc()
            self._memrate = 0.
        return self._memrate
    
    def get_internal_attributes(self):
        al = self.get_device_attr().get_attribute_list()
        return [al[i].get_name() for i in range(len(al))]

    def fireEventsList(self, attr_values={}):
        # self.debug("In %s::fireEventsList()"%self.get_name())
        # @todo: add the value on the push_event

        timestamp = time.time()
        self._event_cache = getattr(self,'_event_cache',
                fn.defaultdict_fromkey(fn.tango.fakeAttributeValue))
        prevs = {a:o.value for a,o in self._event_cache.items()}
        
        if attr_values is None or not len(attr_values):
            attr_values = {}
            al = self.get_internal_attributes()
            self.debug('fireEventsList(%s)' % str(al))
            for a in al:
                try:
                    m = None
                    m = getattr(self,'read_'+a,None)
                    if not m and a in self.values:
                        m = self.read_dyn_attr
                    if m:
                        m(self._event_cache[a])
                        attr_values[a] = self._event_cache[a]
                except:
                    #traceback.print_exc()
                    self.error(a+':'+traceback.format_exc())

        for attr,obj in attr_values.items():
            try:
                value = getattr(obj,'value',obj)
                ##print("In fireEventsList(): {}={}".format(attr,value))
                #print(prevs.get(attr,None))
                quality = getattr(obj,'quality',tango.AttrQuality.ATTR_VALID)
                if value != prevs.get(attr,None):
                    #print(push_change_event({},{})'.format(attr,value))
                    self.push_change_event(attr,value, timestamp, quality)

                self._event_cache[attr].set_value_date_quality(
                        value,timestamp,quality)

            except Exception as e:
                traceback.print_exc()
                
                
    def update_ProcessList(self):
        try:
            self.debug('update_ProcessList(%s)' % self.UseEvents)
            startTime = time.time()
            # self._loadAverage = [] #list(os.getloadavg())
            # self._update_without_thread_split()#self._update_without_thread_split()
            self.update_all_processes()
            self.getMemRate()
            if self.UseEvents:
                self.fireEventsList()
            self.lapseTime = time.time() - startTime
            self.info('Update() finished in %6.3f seconds'%(self.lapseTime))
            return
        except:
            self.error('Unable to launch process list update:\n%s' % (traceback.format_exc()))

    def _update_without_thread_split(self):
        for process in self.ProcessList:
            self.update_process(process)

    def update_disk_usage(self):
        self.disks = [p.mountpoint for p in psutil.disk_partitions()] or ['/']
        self.disk_usage = {d:psutil.disk_usage(d).percent for d in self.disks}
                    
    def update_all_processes(self):
        # tracer('update_all_processes()')
        self.update_disk_usage()
        self.all_proc = get_all_process()
        p = get_worse_process(self.all_proc, 'rss')
        self.maxrss = self.all_proc[p]['rss']
        self.maxrsspid = p
        self.maxrssname = self.all_proc[p]['cmd']
        for process in self.ProcessList:
            self.update_process(process)
        self.uptime = shell_command('uptime')
        

    def update_process(self, process):
        # tracer('update_process(%s)'%process)
        aname = re.sub('[^0-9a-zA-Z]+', '_', process)
        try:
            processes = [k for k, v in self.all_proc.items()
                         if not v['cmd'].lower().startswith('screen ') and re.search(process, v[
                    'cmd'])]  # get_processes_pid(process)
            # self.debug('\t#processes: %d'%len(processes))
            nthreads = 0
            cpu = 0
            mem = 0
            vmem = 0
            memRatio = 0
            if not processes == []:
                for each_process in processes:
                    subthreads, subcpu, submemRatio, submem, subvmem = [self.all_proc[each_process][k] for k in (
                        'threads', 'cpu', 'mem', 'rss', 'vsize')]  # self.update_subprocess(each_process)
                    nthreads += subthreads
                    cpu += subcpu
                    mem += submem
                    vmem += subvmem
                    memRatio += submemRatio
                # self.debug('\ttotal #threads: %d'%nthreads)
                # self.debug('\ttotal cpu: %f'%cpu)
                # self.debug('\ttotal mem: %f'%mem)
                # self.debug('\ttotal vmem: %f'%vmem)
                # self.debug('\ttotal mem ratio: %f'%memRatio)
            self.values[aname + '_nprocesses'] = len(processes)
            self.values[aname + '_nthreads'] = nthreads
            self.values[aname + '_pids'] = processes
            self.values[aname + '_cpu'] = cpu
            self.values[aname + '_mem'] = mem
            self.values[aname + '_vmem'] = vmem
            self.values[aname + '_memRatio'] = memRatio

            if aname not in self.leaks:
                self.leaks[aname] = (time.time(), float(mem), 0)
            elif (self.leaks[aname][0] + 600) < time.time():
                self.leaks[aname] = (
                    time.time(), mem, 60 * 1e3 * (mem - self.leaks[aname][1]) / (time.time() - self.leaks[aname][0]))
            self.values[aname + '_kbpm'] = self.leaks[aname][-1]

            if self.UseEvents:
                events = []
                for suffix in ['nprocesses', 'nthreads', 'pids', 'cpu', 'mem', 'vmem', 'memratio', 'kbpm']:
                    events.append([aname + '_' + suffix, self.values[aname + '_' + suffix]])
                events = [a for a in self.get_internal_attributes() if a.lower() in events]
                self.fireEventsList(events)
        except Exception:
            self.failed.pop(aname, None)
            self.error('Unable to get values for process %s:\n%s' % (process, traceback.format_exc()))
            self.failed[aname] = process
        return
    

    def change_state(self, newstate):
        # self.debug("In %s::change_state(%s)"%(self.get_name(),str(newstate)))
        if self.get_state != newstate:
            self.set_state(newstate)
            if self.UseEvents:
                self.push_change_event('State', newstate)

    #@memory_checker
    def addStatusMsg(self, current, important=False):
        # self.debug("In %s::addStatusMsg()"%self.get_name())
        msg = "The device is in %s state.\n" % (self.get_state())
        for ilog in self._important_logs:
            msg = "%s%s\n" % (msg, ilog)
        status = "%s%s\n" % (msg, current)
        if self.get_status() != status:
            self.set_status(status)
            if self.UseEvents: 
                self.push_change_event('Status', status)
        if important and not current in self._important_logs:
            self.info('writing logs ...')
            self._important_logs.append(current)

    #@memory_checker
    def read_dyn_attr(self, attr):
        aname = attr.get_name()
        value = self.values.get(aname, None)
        self.debug('In read_dyn_attr(%s) = %s ; (mem = %s)' 
                % (aname, value, get_memory()))
        if value == None:
            attr.set_quality(tango.AttrQuality.ATTR_INVALID)
            attr.set_value(0)
        #elif attr.get_data_format() == tango.SPECTRUM:
        elif fn.isSequence(value): 
            attr.set_quality(tango.AttrQuality.ATTR_VALID)
            attr.set_value(value, len(value))
        #attr.get_data_format() == tango.SCALAR:
        else:
            attr.set_quality(tango.AttrQuality.ATTR_VALID)
            attr.set_value(value)


    # Needed to have proper dynamic attributes
    #@memory_checker
    def dyn_attr(self):
        for process in self.ProcessList:
            # self.debug('dyn_attr(): Adding attributes for %s process'%process)
            aname = re.sub('[^0-9a-zA-Z]+', '_', process)
            for suffix, data_type, data_format, unit in \
                    (('nprocesses', tango.DevLong, tango.SCALAR, None),
                     ('nthreads', tango.DevLong, tango.SCALAR, None),
                     ('pids', tango.DevLong, tango.SPECTRUM, None),
                     ('cpu', tango.DevDouble, tango.SCALAR, '%'),
                     ('mem', tango.DevDouble, tango.SCALAR, 'MB'),
                     ('vmem', tango.DevDouble, tango.SCALAR, 'MB'),
                     ('memRatio', tango.DevDouble, tango.SCALAR, '%'),
                     ('kbpm', tango.DevDouble, tango.SCALAR, '%'),
                     ):
                # TODO: set the units
                if data_format == tango.SCALAR:
                    self.add_attribute(tango.Attr(aname + '_' + suffix,
                                                  data_type,
                                                  tango.AttrWriteType.READ),
                                       self.read_dyn_attr, None,
                                       (lambda req_type, attr_name=aname + '_' + suffix: True))
                    if self.UseEvents:
                        self.set_change_event(aname + '_' + suffix, True, False)

                elif data_format == tango.SPECTRUM:
                    self.add_attribute(tango.SpectrumAttr(aname + '_' + suffix,
                                                          data_type,
                                                          tango.READ, 1000),
                                       self.read_dyn_attr, None,
                                       (lambda req_type, attr_name=aname + '_' + suffix: True))
                    if self.UseEvents:
                        self.set_change_event(aname + '_' + suffix, True, False)
        # self._dyn_attr_build = True
        self.change_state(tango.DevState.ON)
        return

    # ------------------------------------------------------------------
    #    Device constructor
    # ------------------------------------------------------------------
    def __init__(self, cl, name):
        fn.device.Dev4Tango.__init__(self, cl, name)
        ProcessProfiler.init_device(self)

    # ------------------------------------------------------------------
    #    Device destructor
    # ------------------------------------------------------------------
    def delete_device(self):
        # self.debug("[Device delete_device method] for device",self.get_name())
        return

    # ------------------------------------------------------------------
    #    Device initialization
    # ------------------------------------------------------------------
    def init_device(self):
        # self.debug("In ", self.get_name(), "::init_device()")
        self.set_state(tango.DevState.INIT)
        self.get_device_properties(self.get_device_class())

        if self.UseEvents:
            for a in ProcessProfilerClass.attr_list:
                self.set_change_event(a, True, False)

        self._important_logs = []
        self._loadAverage = []
        self.values = fn.CaselessDict()
        self.failed = {}
        self.leaks = {}  # For each process time,memory,leak/minute will be recorded every 600s.
        self._PID = os.getpid()
        self._memrate = 0.
        self.updateThread = None
        self.lapseTime = None
        self.maxrss, self.maxrsspid, self.maxrssname = 0, 0, ''
        self._total, self._free, self._cached, self._used = 1, 0, 0, 0
        self._nTasks = 0
        
        self.os_version = shell_command('cat /etc/issue | head -1').strip()
        self.cpus = [l for l in (_.split(':')[-1].strip() for _ in 
            shell_command('cat /proc/cpuinfo | grep "model name"').split('\n'))
            if l]
        self._nCPUs = len(list(self.cpus))
        self.kernel = shell_command('uname -r').strip()
        self._memrate = 0.
        self.getMemRate()
        self.uptime = shell_command('uptime')
        self.update_all_processes()
        
        print('Ready to receive request ...\n\n')
        self.dyn_attr()  # necessary to re-populate dynamic attributes 
        # if the property list have changed and device Init().

    # ------------------------------------------------------------------
    #    Always excuted hook method
    # ------------------------------------------------------------------
    #@memory_checker
    def always_executed_hook(self):
        try:
            # self.debug("In ", self.get_name(), "::always_excuted_hook()")
            default = 'Device is %s' % self.get_state()
            if self.failed:
                msg = 'Process not found:]\n%s' % '\n'.join(
                    sorted(str(t) for t in self.failed.items()))
                self.set_status(msg)
                print(self.get_status())

            else:
                status = default
                status += '\nTotal: %s, Used: %s' % (self._total, self._used)
                status += '\nFree: %s, Cached: %s' % (self._free, self._cached)
                status += '\nLoad: %s' % str(self._loadAverage)
                status += '\nMaxRss: PID %s (%s kb)\n%s' % (
                    self.maxrsspid, self.maxrss, self.maxrssname)
                self.set_status(status)

        except:
            self.set_status(traceback.format_exc())
            self.set_state(tango.DevState.UNKNOWN)
            print(self.get_status())

    # ==================================================================
    #
    #    ProcessProfiler read/write attribute methods
    #
    # ==================================================================
    # ------------------------------------------------------------------
    #    Read Attribute Hardware
    # ------------------------------------------------------------------
    def read_attr_hardware(self, data):
        # self.debug("In ", self.get_name(), "::read_attr_hardware()")
        pass 

    def read_MaxRss(self, attr):
        # self.debug("In %s::read_MaxRss()"%self.get_name())
        attr.set_value(self.maxrss)

    def read_MaxRssProcess(self, attr):
        # self.debug("In %s::read_MaxRssProcess()"%self.get_name())
        attr.set_value(self.maxrssname)

    # ------------------------------------------------------------------
    #    Read LoadAverage attribute
    # ------------------------------------------------------------------
    def read_LoadAverage(self, attr):
        self.debug("In %s::read_LoadAverage()" % self.get_name())

        #    Add your own code here

        attr.set_value(self._loadAverage, len(self._loadAverage))

    # ------------------------------------------------------------------
    #    Read nCPUs attribute
    # ------------------------------------------------------------------
    def read_nCPUs(self, attr):
        # self.debug("In %s::nCPUs()"%self.get_name())

        #    Add your own code here

        attr.set_value(self._nCPUs)

    def read_UpdateLapseTime(self, attr):
        # self.debug("In %s::UpdateLapseTime()"%self.get_name())

        #    Add your own code here

        attr.set_value(self.lapseTime)

    # ------------------------------------------------------------------
    #    Read MemUsage attribute
    # ------------------------------------------------------------------
    def read_MemUsage(self, attr=None):
        # self.debug("In read_MemUsage()")

        #    Add your own code here
        if self._PID in self.all_proc:
            v = self.all_proc[self._PID]['rss']
        else:
            v = self.getMemUsage()
        if attr is not None:
            attr.set_value(v)
        return v

    # ------------------------------------------------------------------
    #    Read MemRate attribute
    # ------------------------------------------------------------------

    def read_MemRate(self, attr):
        # self.debug("In read_MemRate()")

        #    Add your own code here
        attr.set_value(self._memrate)

    # ------------------------------------------------------------------
    #    Read NTasks attribute
    # ------------------------------------------------------------------

    def read_NTasks(self, attr):
        # self.debug("In read_NTasks()")

        #    Add your own code here
        attr.set_value(self._nTasks)
        
    def read_OS(self, attr): 
        attr.set_value(self.os_version)
        
    def read_CPUModel(self, attr): 
        #print('cpus:',list(self.cpus))
        attr.set_value(first(self.cpus,''))
        
    def read_Kernel(self, attr): 
        attr.set_value(self.kernel)
        
    def read_DiskUsage(self, attr): 
        vs = ['%s:%s%%' % t for t in self.disk_usage.items()]
        attr.set_value(vs)
        
    def read_MaxDiskUsage(self, attr): 
        attr.set_value(max(self.disk_usage.values() or [0]))        
        
    def read_Uptime(self, attr): 
        attr.set_value(self.uptime)        

    # ==================================================================
    #
    #    ProcessProfiler command methods
    #
    # ==================================================================

    # ------------------------------------------------------------------
    #    Update command:
    #
    #    Description: This command will trigger the update of the dynamic attributes values for all listed processes.
    #
    # ------------------------------------------------------------------
    #@memory_checker
    def Update(self):
        self.debug("In %s::Update(%s)" % (self.get_name(), self.Threaded))
        #    Add your own code here
        try:
            if self.get_state() == tango.DevState.INIT:
                # self.warning("Not yet initialized to start an update.")
                return
            self._loadAverage = list(os.getloadavg())
            try:
                self._nTasks = int(shell_command("ps auxwwH|wc -l"))
            except:
                traceback.print_exc()
                self._nTasks = 0

            # do in a separate thread to avoid to hang the device if it takes too long
            if self.Threaded:
                if not self.updateThread == None and self.updateThread.is_alive():
                    # self.warning("Try to update when not finish yet last call")
                    self.change_state(tango.DevState.ALARM)
                    self.addStatusMsg("Update() command call overlapped with a non finish previous call")
                else:
                    if self.updateThread is not None: del self.updateThread
                    # self.updateThread = Process(target=self.update_ProcessList)
                    self.updateThread = threading.Thread(target=self.update_ProcessList)
                    self.updateThread.run()
                    self.change_state(tango.DevState.ON)
                    self.addStatusMsg("")
            else:
                self.update_ProcessList()
                self.change_state(tango.DevState.ON)
        except Exception as e:
            self.error("Update() exception: %s" % (traceback.format_exc()))


    # ------------------------------------------------------------------
    #    evaluateFormula command:
    # ------------------------------------------------------------------

    def evaluateFormula(self, argin):
        t0 = time.time()
        if not self.Debug:
            raise Exception('DebugModeDisabled')
        
        #tracer('\tevaluateFormula(%s)' % (argin,))
        try:
            argout = eval(str(argin), globals(), locals())
        except Exception as e:
            argout = e
        #tracer('\tevaluateFormula took %s seconds' % (time.time() - t0))
        return str(argout)


# ==================================================================
#
#    ProcessProfilerClass class definition
#
# ==================================================================
class ProcessProfilerClass(tango.DeviceClass):
    #    Class Properties
    class_property_list = {
    }

    #    Device Properties
    device_property_list = {
        'ProcessList':
            [tango.DevVarStringArray,
             "List of processes to be monitorized",
             []],
        'UseEvents':
            [tango.DevBoolean,
             "Whether to push events or not",
             [True]],
        'Threaded':
            [tango.DevBoolean,
             "Whether to use threads or not",
             [False]],
        'Debug':
            [tango.DevBoolean,
             "Whether to use threads or not",
             [False]],            
    }

    #    Command definitions
    cmd_list = {
        'Update':
            [[tango.DevVoid, ""],
             [tango.DevVoid, ""],
             {
                 'Polling period': 10000,
             }],
        'evaluateFormula':
            [[tango.DevString, "formula to evaluate"],
             [tango.DevString, "formula to evaluate"],
             {
                 'Display level': tango.DispLevel.EXPERT,
             }],
    }

    #    Attribute definitions
    attr_list = {
        'MaxRss':
            [[tango.DevDouble,
              tango.SCALAR,
              tango.READ],
             {
                 'unit': 'kb',
             }],
        'MaxRssProcess':
            [[tango.DevString,
              tango.SCALAR,
              tango.READ],
             ],
        'LoadAverage':
            [[tango.DevDouble,
              tango.SPECTRUM,
              tango.READ, 3],
             {
                 'label': "LoadAverage",
                 'description': "one, five, and fifteen minute averages",
             }],
        'nCPUs':
            [[tango.DevShort,
              tango.SCALAR,
              tango.READ],
             {
                 'label': 'CPUs',
                 'description': 'Number of CPUs in this machine',
             }],
        'UpdateLapseTime':
            [[tango.DevDouble,
              tango.SCALAR,
              tango.READ],
             {
                 'label': 'Update lapsetime',
                 'description': 'Time used by the Update() command to proceed',
                 'unit': 's',
             }],
        'MemUsage':
            [[tango.DevDouble,
              tango.SCALAR,
              tango.READ],
             {
                 'description': 'This attribute returns the memory of OWN process',
                 'unit': 'kb',
             }],
        'MemRate':
            [[tango.DevDouble,
              tango.SCALAR,
              tango.READ],
             {
                 'description': 'This attribute returns total-(free+cached) memory',
                 'unit': ' ',
             }],
        'NTasks':
            [[tango.DevLong,
              tango.SCALAR,
              tango.READ],
             {
                 'description': 'Number of Tasks/Threads on this host',
                 'unit': ' ',
             }],
       'Kernel':
           [[tango.DevString,
           tango.SCALAR,
           tango.READ],
            {
                'description': '',
                'unit':' ',
            } ],
       'DiskUsage':
           [[tango.DevString,
           tango.SPECTRUM,
           tango.READ, 256],
            {
                'description': '',
                'unit':' ',
            } ],
       'OS':
           [[tango.DevString,
           tango.SCALAR,
           tango.READ],
            {
                'description': '',
                'unit':' ',
            } ],
       'MaxDiskUsage':
           [[tango.DevDouble,
           tango.SCALAR,
           tango.READ],
            {
                'description': '',
                'unit':' ',
            } ],
        'CPUModel':
           [[tango.DevString,
           tango.SCALAR,
           tango.READ],
            {
                'description': '',
                'unit':' ',
            } ],   
       'Uptime':
           [[tango.DevString,
           tango.SCALAR,
           tango.READ],
            {
                'description': '',
                'unit':' ',
            } ],            
    }

    # Needed to have proper dynamic attributes
    def dyn_attr(self, dev_list):
        for dev in dev_list:
            try:
                dev.dyn_attr()
            except:
                print(traceback.format_exc())

    # ------------------------------------------------------------------
    #    ProcessProfilerClass Constructor
    # ------------------------------------------------------------------
    def __init__(self, name):
        tango.DeviceClass.__init__(self, name)
        self.set_type(name)
        print("In ProcessProfilerClass  constructor")


# ==================================================================
#
#    ProcessProfiler class main method
#
# ==================================================================
if __name__ == '__main__':
    try:
        py = tango.Util(sys.argv)
        py.add_TgClass(ProcessProfilerClass, ProcessProfiler, 'ProcessProfiler')

        U = tango.Util.instance()
        U.server_init()
        U.server_run()

    except tango.DevFailed as e:
        print('-------> Received a DevFailed exception:', e)
    except Exception as e:
        print('-------> An unforeseen exception occured....', e)
