
from fandango.tango import *

hosts = [d.split('/')[-1] for d in get_class_devices('Starter')]

for h in hosts:
    s = 'ProcessProfiler/' + h
    d = 'sys/profile/%s' % h
    if not check_device(d):
        print('creating %s' % d)
        add_new_device(s, 'ProcessProfiler', d)
        astor = fn.Astor(s)
        astor.start_servers(s,host=h)
        wait(2.)
        if check_device(d):
            astor.set_server_level(s,h,1)

try:    
    import panic
    alarms = panic.api()
    devs = filtersmart(alarms.devices.keys(),'/alarms$')
    if len(devs)==1:
        print('creating memory usage alarm ...')
        alarms.add('MEMORY_USAGE',devs[0],
            '(any([m>0.7 for m in FIND(sys/profile/*/memrate)]) '
            ' or any([m>80 for m in FIND(sys/profile/*/maxdiskusage)]))'
            ' and FIND(sys/profile/*/max*) and FIND(sys/profile/*/load*)',
            'memory usage is above 70%% in a control host')
except:
    print('unable to configure alarms: \n' + traceback.format_exc())
    
try:
    import PyTangoArchiving
    hdbpp = PyTangoArchiving.api('hdbpp')
    attrs = find_attributes('sys/profile/*/(max|load|mem)*')
    attrs = [a for a in attrs if not hdbpp.is_attribute_archived(a)]
    if attrs:
        print('adding attributes to archiving ...')
        hdbpp.add_attributes(attrs,code_event=True)
except:
    print('unable to configure archiving: \n'+ traceback.format_exc())


    
